[![License: Apache 2.0](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)

Docker - Alpine Samba Domain Controller
=======================================

This image provides a Samba 4 Domain Controller using an Alpine Linux base.

Configuration
=============
First, Pull the image:

```bash
docker pull saltyvagrant/samba-dc:0.1.0
```

Now, start the image with the correct environment variables for initial
configuration:

* Note that we're persisting the samba volume to a local directory named
`samba` in your working dir

```bash
mkdir ${PWD}/samba

docker run -d --restart unless-stopped \
    -e SAMBA_DC_REALM='corp.example.net' \
    -e SAMBA_DC_DOMAIN='EXAMPLE' \
    -e SAMBA_DC_ADMIN_PASSWD='5u3r53cur3!' \
    -e SAMBA_DC_DNS_BACKEND='SAMBA_INTERNAL' \
    -v ${PWD}/samba:/samba \
     'saltyvagrant/samba-dc:0.1.0'
```

Usage
=====
After the container has been run for the first time, invoke it with the following command

```bash
docker run -d --restart unless-stopped -v ${PWD}/samba:/samba 'saltyvagrant/samba-dc:0.1.0'
```

Volumes
=======

The following volumes are exposed:


| Name | Value | Description |
|------|-------|-------------|
| Samba | /samba | Re-homed Samba Config, data and log directory |

Environment Variables
=====================

The following environment variables are available for your configuration
pleasure:

| Name | Default | Description |
|------|---------|-------------|
| SAMBA_DNS_FORWAARDER | 1.1.1.1 | DNS frowarder to use when using `SAMBA_INTERNAL`
| SAMBA_DC_REALM | corp.example.net | The realm to launch the DC into
| SAMBA_DC_DOMAIN | EXAMPLE | The NetBIOS Domain Name
| SAMBA_DC_ADMIN_PASSWD | 5u3r53cur3! | The AD DC `Administrator` user password
| SAMBA_DC_DNS_BACKEND | SAMBA_INTERNAL | The DNS backend to use

Bug Tracker
===========

Bugs are tracked on [GitLab Issues](https://gitlab.com/svdocker/samba-dc/issues).
In case of trouble, please check there to see if your issue has already been reported.
If you spotted it first, help us smash it by providing detailed and welcomed feedback.

Credits
=======

Maintainer
----------

This module is maintained by [Salty Vagrant](https://saltyvagrant.com)

* https://gitlab.com/svdocker/samba-dc
